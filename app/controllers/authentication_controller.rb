class AuthenticationController < ApplicationController
 # before_action :authorize_request, except: :login
 # skip_before_action :verify_authenticity_token

  
  require "json_web_token"

  def login
    @user = User.find_by_email(params[:email])
    if @user&.authenticate(params[:password])&.is_active == true
      token = JsonWebToken.encode(user_id: @user.id)
      time = Time.now + 2.hours.to_i
      render json: { token: token, exp: time.strftime("%m-%d-%Y %H:%M"),
                     user_type: @user.user_type, user_id: @user.id, is_active: @user.is_active }, status: :ok
    else
      render json: { error: 'unauthorized',is_active: false }, status: :unauthorized
    end
  end

  private

  def login_params
    params.permit(:email, :password)
  end
end