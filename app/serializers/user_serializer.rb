class UserSerializer < ActiveModel::Serializer
  attributes :id,:name,:email,:city,:product_count,:role,:product_list

def product_count
	object.products.count
end


def product_list
	User.where(user_type: 2).map{|i| i.products}
end


end
