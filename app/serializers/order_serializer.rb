class OrderSerializer < ActiveModel::Serializer
  attributes :id,:order_date,:quantity,:user,:product,:order_date

  def user
  	user_id = object.user
  end

  def product
  	product_id = object.product
  end

  def order_date
    object.order_date
  end


  def image
  	object.image
  end


end
