class ProductSerializer < ActiveModel::Serializer
  attributes :id,:name,:price,:stock,:category,:user,:image

def product_count
	User.all.map {|i| i.products.count}
end

def order_date
	object.order.order_date
end
	
def image
	object.image
end

end
