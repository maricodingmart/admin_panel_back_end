class OrdersController < ApplicationController

	def address_create
		OrderDelivery.create(name: params[:name],phone: params[:phone],address: params[:address])
	end

	def order_all
		if User.where(id: params[:id]).present?
			order = Order.where(user_id: params[:id])
			render json: order
		else
			render json: "User not present"
		end
	end

	def cart_delete
		cart = Order.find(params[:id]).delete
		render json: "Cart has been successfully deleted"
	end

	def customer_orders
		if User.where(id: params[:id]).present?
			order_data  = Order.where(user_id: params[:id]).map { |i| i.product  }
			render json: order_data
		else
			render json: "User not present"
		end
	end

	def shipping
		if Order.where(id: params[:id]).present?
			cart = Order.find(params[:id]).product
			render json: cart
		else
			render json: "Product is not present"
		end
	end

	def cart_create
		Cart.create(quantity: params[:quantity],user_id: params[:user_id])
		Order.create(order_date: Time.now,quantity: params[:quantity],user_id: params[:user_id],product_id: params[:product_id],order_delivery_id: params[:order_delivery_id])
		Product.find(params[:product_id]).update(stock: Product.find(params[:product_id]).stock.to_i - (params[:quantity].to_i))
	end

	def address_all
		order = OrderDelivery.all
		render json: order
	end

	def address_update
		order = OrderDelivery.find(params[:id])
		if order.update(order_delivery_params)
			render json: {result: "true",user: order.user.id}
		else
			render json: "false"
		end
	end


private

  def order_delivery_params
  	params.permit(:name,:phone,:address)
  end

end
