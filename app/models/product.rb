class Product < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :carts
  has_many :orders #dependent: :destroy

   has_attached_file \
    :image,
    styles: { thumb: ['1000x1000#', 'jpg'] },
    convert_options: {
      all: '-interlace Plane'
    },
    default_url: '/images/default_:style_photo.png'
  validates_attachment_presence :image
  validates_attachment_file_name :image, matches: [/png\Z/, /jpe?g\Z/, /gif\Z/]




end
