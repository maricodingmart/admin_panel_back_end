class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :role
      t.string :city
      t.integer :user_type
      t.boolean :is_active
      
      t.timestamps
    end
  end
end
