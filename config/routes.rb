Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users, param: :_username
  post '/auth/login', to: 'authentication#login'
  get 'product_list' => 'products#product_list'
  post 'product_create' => 'products#product_create'
  post 'product_update' => 'products#product_update'
  get 'category_list' => 'products#category_list'
  post 'product_delete' => 'products#product_delete'
  get 'product_all' => 'products#product_all'
  post 'supplier_create' => 'products#supplier_create' 
  get 'seller_list' => 'users#seller_list'
  get 'customer_list' => 'users#customer_list'
  post 'address_create' => 'orders#address_create'
  post 'cart_create' => 'orders#cart_create'
  get 'address_all' => 'orders#address_all'
  get 'category_all' => 'users#category_all'
  get 'order_all' => 'orders#order_all'
  post 'cart_delete' => 'orders#cart_delete'
  get 'shipping' => 'orders#shipping'
  get 'customer_orders' => 'orders#customer_orders'
    get 'all_products' => 'products#all_products'
  
end
