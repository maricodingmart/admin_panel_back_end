class ProductsController < ApplicationController
 # skip_before_action :login, only: %i[product_list]
 # skip_before_action :verify_authenticity_token

	def product_list
		if Product.where(id: params[:id]).present?
			product = Product.find(params[:id])
			render json: product
		else
			render json: {product: "Product is not present"}
		end
	end

	def all_products
		product = Product.all
		render json: product
	end

	def product_delete
		Product.find(params[:id]).delete
	end

	def product_all
		if User.where(id: params[:id]).present?
			products = Product.where(user_id: params[:id])
			render json: products
		else
			render json: {products: "User has no Products"}
		end
	end

	def category_list
		category = Category.all.pluck(:category_name)
		render json: category
	end

	def product_create
		product = Product.create(name: params[:name], price: params[:price], stock: params[:stock], user_id: params[:supplier_id], category_id: params[:category_id],image: params[:image])
		render json: product
	end

	def product_update
		product = Product.find(params[:id])
		if product.update(product_params)
			render json: {result: "true",user: product.user.id}
		else
			render json: "false"
		end
	end


	  private

  def product_params
  	params.permit(:name,:price,:stock,:user_id,:category_id,:image)
  end

end
