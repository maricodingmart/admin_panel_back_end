class UsersController < ApplicationController
  #before_action :authorize_request, except: :create
  #before_action :find_user, except: %i[create index]

  # GET /users
  def index
    @users = User.all
    render json: @users, status: :ok
  end

  def category_all
    category = Category.all
    render json: category
  end


  def customer_list
    if User.all.present?
      @users = User.all.select {|i| i.user_type == 3}
      render json: @users, status: :ok
    else
      render json: "There are no Customer list"
    end
  end


  def seller_list
    if User.all.present?
      @users = User.all.select {|i| i.user_type == 2}
      render json: @users, status: :ok
    else
        render json: "There are no seller list"
    end
  end


  # GET /users/{username}
  def show
    render json: @user, status: :ok
  end

  # POST /users
  def create
    user = User.create(name: params[:name],email: params[:email],password: params[:password],user_type: params[:role],is_active: false)
  end

  # PUT /users/{username}
  def update
    unless @user.update(user_params)
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /users/{username}
  def destroy
    @user.destroy
  end

  private

  def find_user
    @user = User.find_by_name!(params[:_name])
    rescue ActiveRecord::RecordNotFound
      render json: { errors: 'User not found' }, status: :not_found
  end

  def user_params
    params.permit(
       :name, :email, :password, :password_confirmation, :role, :user_type
    )
  end
end